FROM bigbluebutton/greenlight:v2

COPY patches /patches
RUN apk add -U patch && \
    for patchfile in /patches/*.patch; do patch -p1 -i $patchfile; done; \
    rm -rf /patches

ADD entrypoint /entrypoint
ENTRYPOINT ["/entrypoint"]
